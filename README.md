# ide-auto-completion

The script, when executed in Studio or Batch generates IDE packages for auto-completion outside of Pixyz. Files are generated in `%AppData%/PiXYZStudio`.

## How to use

* Open `generate_auto_completion_libs.py` in Studio
* Execute
* Get the generated libs at `%AppData%/PiXYZStudio`
* Then, follow the instalation guideline for your favorite IDE

### Visual Code

Copy **python.json** at *%appdata%/Code/User/snippets/*

### Sublime Text 3

Copy **pixyz.sublime-completions** at *%appdata%/Sublime Text 3/Packages/User/*

### PyCharm

Install **PiXYZ-PyCharm-AutoCompletion.zip** in PyCharm: *File > Import Settings...*
